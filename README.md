# SignKits
This plugin that allows players to save and load inventories from signs.

## Use
Create a sign with the first line as either 
<pre>
[SaveKit]
</pre>
or
<pre>
[LoadKit]
</pre>

Make the second line a number from 1-10.
<br> Every player has 10 kits.

## Permissions
<pre>
permissions:
  signkits.*:
    description: Gives access to creating and using SignKits signs.
  signkits.create:
    description: Allows a player to create a load or save sign.
    default: op
  signkits.use:
    description: Allows a player to load or save kits.
    default: true
</pre>

## Commands
This plugin has no commands.
