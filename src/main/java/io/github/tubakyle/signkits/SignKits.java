package io.github.tubakyle.signkits;

import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;

public final class SignKits extends JavaPlugin implements Listener {
    public void onEnable() {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    void onCreateSign(SignChangeEvent e) {
        Player p = e.getPlayer();
        if (e.getLine(0).equalsIgnoreCase("[SaveKit]") || e.getLine(0).equalsIgnoreCase("[LoadKit]")) {
            if (p.hasPermission("signkits.create")) {
                List<String> num = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
                if(num.contains(e.getLine(1))) {
                    String line0;
                    if (e.getLine(0).equalsIgnoreCase("[SaveKit]")) {
                        line0 = "[SaveKit]";
                        sendMessage(p, ChatColor.GREEN + "SaveKit sign successfully created.");
                    } else {
                        line0 = "[LoadKit]";
                        sendMessage(p, ChatColor.GREEN + "LoadKit sign successfully created.");
                    }
                    e.setLine(0, ChatColor.DARK_BLUE + line0);
                } else {
                    sendMessage(p, ChatColor.RED + "Line 2 must be an integer from 1-10.");
                    e.setLine(0, "");
                }
            } else {
                e.setLine(0, "");
                sendMessage(p, ChatColor.RED + "You don't have permission for that.");
            }

        }
    }

    @EventHandler
    void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getState() instanceof Sign) {
            Sign s = (Sign) e.getClickedBlock().getState();
            if (s.getLine(0).equalsIgnoreCase(ChatColor.DARK_BLUE + "[SaveKit]") || s.getLine(0).equalsIgnoreCase(ChatColor.DARK_BLUE + "[LoadKit]")) {
                if (p.hasPermission("signkits.use")) {
                    int kitNum = Integer.parseInt(s.getLine(1));
                    if(s.getLine(0).equalsIgnoreCase(ChatColor.DARK_BLUE + "[SaveKit]")) {
                        saveKit(p, kitNum);
                    } else if (s.getLine(0).equalsIgnoreCase(ChatColor.DARK_BLUE + "[LoadKit]")) {
                        loadKit(p, kitNum);
                    }
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    public void loadKit(Player p, int kitNum) {
        reloadConfig();
        String path = p.getUniqueId().toString() + "." + kitNum + ".inventory";
        String armorPath = p.getUniqueId().toString() + "." + kitNum + ".armor";
        if (getConfig().isSet(path)) {
            List<ItemStack> invToLoad = (List<ItemStack>)getConfig().get(path);
            List<ItemStack> armorToLoad = (List<ItemStack>)getConfig().get(armorPath);
            ItemStack[] inv = invToLoad.toArray(new ItemStack[0]);
            ItemStack[] armor = armorToLoad.toArray(new ItemStack[0]);
            p.getInventory().setContents(inv);
            p.getInventory().setArmorContents(armor);
            p.updateInventory();
            sendMessage(p, ChatColor.GREEN + "Kit #" + kitNum + " successfully loaded.");
        } else {
            sendMessage(p, ChatColor.RED + "You don't have a kit #" + kitNum + " saved.");
        }
    }

    public void saveKit(Player p, int kitNum) {
        String path = p.getUniqueId().toString() + "." + kitNum + ".inventory";
        getConfig().set(path, p.getInventory().getContents());
        String armorPath = p.getUniqueId().toString() + "." + kitNum + ".armor";
        getConfig().set(armorPath, p.getInventory().getArmorContents());
        saveConfig();
        sendMessage(p, ChatColor.GREEN + "Kit #" + kitNum + " successfully saved.");
    }

    void sendMessage(Player p, String msg) {
        p.sendMessage(ChatColor.BLUE + "[SignKits] " + ChatColor.WHITE + msg);
    }
}